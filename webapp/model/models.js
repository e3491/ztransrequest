/* global oComponent_TR : true */
/* global isAttachRequest : true */
/* global oModels : true */
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"sap/ui/model/Filter",
	'sap/m/MessageBox',
	"ztransrequest/model/popovers"
], function(JSONModel, Device, Filter, MessageBox, popovers ) {
	"use strict";

	return {
		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createFLPModel: function() {
			var fnGetuser = jQuery.sap.getObject("sap.ushell.Container.getUser"),
				bIsShareInJamActive = fnGetuser ? fnGetuser().isJamActive() : false,
				oModel = new JSONModel({
					isShareInJamActive: bIsShareInJamActive
				});
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		
		createGeneralModel: function () {
			//var userInfo = new sap.ushell.services.UserInfo().getUser();
			var oModel = new sap.ui.model.json.JSONModel({
				general: {
					currentRout: "",
					expandFilters: false,
					mode: "view"
				},
				fullScreen: false,
				dispalyItemsTable: false,
				billLoad : "",
				aReqItems: [],
				aMatnrList: []
			});
			oModel.setSizeLimit(10000);
			return oModel;
		},
		createMasterModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
				SelectedFilters:{
					freeText:"",
					statusKey: "ALL STATUS"
				},
			});
			oModel.setSizeLimit(10000);
			return oModel;
		},
		createDetailsModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
				currReq:{},
				transferStockEnabled: false,
				ConfrimStockEnabled: false
			});
			oModel.setSizeLimit(10000);
			return oModel;
		},
		createCreaterModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
				currReq: {stocLocEnabled: true}
			});
			oModel.setSizeLimit(10000);
			return oModel;
		},
			//-----------------  M O D E L    M E T H O D S  -----------------//

			attachModelEvents: function () {
				oComponent_TR.getModel("RESER").attachRequestSent(function () {
					sap.ui.core.BusyIndicator.show();
				});
	
				oComponent_TR.getModel("CA").attachRequestSent(function () {
					sap.ui.core.BusyIndicator.show();
				});

				oComponent_TR.getModel("MASTERDATA").attachRequestSent(function () {
					sap.ui.core.BusyIndicator.show();
				});
	
				oComponent_TR.getModel("RESER").attachRequestCompleted(function () {
					if (isAttachRequest)
						sap.ui.core.BusyIndicator.hide();
				});
				oComponent_TR.getModel("CA").attachRequestCompleted(function () {
					if (isAttachRequest)
						sap.ui.core.BusyIndicator.hide();
				});
				oComponent_TR.getModel("MASTERDATA").attachRequestCompleted(function () {
					if (isAttachRequest)
						sap.ui.core.BusyIndicator.hide();
				});
	
				oComponent_TR.getModel("RESER").attachRequestFailed(function () {
					isAttachRequest = true;
					sap.ui.core.BusyIndicator.hide();
				});
				oComponent_TR.getModel("CA").attachRequestFailed(function () {
					isAttachRequest = true;
					sap.ui.core.BusyIndicator.hide();
				});
				oComponent_TR.getModel("MASTERDATA").attachRequestFailed(function () {
					isAttachRequest = true;
					sap.ui.core.BusyIndicator.hide();
				});
			},
			handleErrors: function (error) {
				try {
					MessageBox.error(JSON.parse(error.responseText).error.message.value);
				} catch (e) {
					MessageBox.error(JSON.stringify(error));
				}
			},

			//-----------------  G E N E R I C    C R U D  -----------------//

			genericQuery: function (sModel, sPath, aFilters, oCurrComponent) {
				var that = this;
				return new Promise((resolve, reject) => {
					oCurrComponent.getModel(sModel).read(sPath, {
						filters: aFilters,
						success: (data) => {
							resolve(data);
						},
						error: (error) => {
							that.handleErrors(error);
							reject(error);
						}
					});
				});
			},
			genericRead: function (sModel, sKey, oCurrComponent) {
				var that = this;
				return new Promise(function (resolve, reject) {
					oCurrComponent.getModel(sModel).read(sKey, {
						success: (data) =>  {
							resolve(data);
						},
						error: (error) => {
							that.handleErrors(error);
							reject(error);
						}
					});
				});
			},
			genericReadWithUrlParams: function (sModel, sKey, oCurrComponent, urlParameters) {
				var that = this;
				return new Promise(function (resolve, reject) {
					oCurrComponent.getModel(sModel).read(sKey, {
						urlParameters: urlParameters,
						success: (data) => {
							resolve(data);
						},
						error: (error) => {
							that.handleErrors(error);
							reject(error);
						}
					});
				});
			},
			genericDeepInsert: function (sModel, sPath, oEntry, oCurrComponent) {
				var that = this;
				return new Promise((resolve, reject) => {
					oCurrComponent.getModel(sModel).create(sPath, oEntry, {
						success: (data) => {
							resolve(data);
							},
						error: (error) => {
							that.handleErrors(error);
							reject(error);
						}
					});
				});
			}
	};

});