/*global true oComponent_TR*/
sap.ui.define([], function() {
	"use strict";

	return {
		getColorText: function (StockWOutSs,Request,RequiredQntSum) {
			if ((Number(StockWOutSs) < Number(Request)) && (Number(Request) === Number(RequiredQntSum))) {
				return 'Red';
			}else if((Number(StockWOutSs) > Number(Request)) && (Number(Request) !== Number(RequiredQntSum))){
				return 'Pink';
			}else if((Number(StockWOutSs) < Number(Request)) && (Number(Request) !== Number(RequiredQntSum))	){
				return 'Blue';
			}else{
				return 'Black';
			}
		},
	};

});