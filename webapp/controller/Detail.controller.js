/* global oComponent_TR: true */
sap.ui.define([
	"ztransrequest/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"

], function (BaseController, JSONModel, Controller, MessageBox) {
	"use strict";

	return BaseController.extend("ztransrequest.controller.Detail", {
		onInit: function () {
			oComponent_TR._detailController = this;
			this.oRouter = this.getOwnerComponent().getRouter();
			this.oModel = this.getOwnerComponent().getModel();

			//this.oRouter.getRoute("detail").attachPatternMatched(this._onProductMatched, this);
			this.oRouter.getRoute("view_req").attachPatternMatched(this._onViewMatched, this);
			this.oRouter.getRoute("edit_req").attachPatternMatched(this._onEditMatched, this);
			this.oRouter.getRoute("detailFull").attachPatternMatched(this._onFullMatched, this);
		},
		_onFullMatched: function (oEvent) {
			oComponent_TR.getModel("General").setProperty("/fullScreen",true);
			oComponent_TR._fcl.byId("fcl").setLayout("MidColumnFullScreen");
			oComponent_TR._fcl._updateUIElements();
		},
		handleExitFullScreen: function (oEvent) {
			oComponent_TR.getModel("General").setProperty("/fullScreen",false);
			oComponent_TR._fcl.byId("fcl").setLayout("TwoColumnsMidExpanded");
			oComponent_TR._fcl._updateUIElements();
		},
		_onViewMatched: function (oEvent) {
			this._reqId = oEvent.getParameter("arguments").reqId;
			oComponent_TR.getModel("Details").setProperty("/reqId",this._reqId);
			oComponent_TR._fcl.byId("fcl").setLayout("TwoColumnsMidExpanded");
			oComponent_TR._fcl._updateUIElements();

			let generalModel = oComponent_TR.getModel("General");
			generalModel.setProperty("/general/mode", "view");
			generalModel.setProperty("/dispalyItemsTable", true);

			//get header and items for rsum selection screen
			this.getRsnumDetails();
		},
		getRsnumDetails: function () {
			var modelGenral = oComponent_TR.getModel("General"),
			modelDetails = oComponent_TR.getModel("Details"),
			sStatus = modelGenral.getProperty("/status"),
			aReqItems = [];

			let sKey = oComponent_TR.getModel("RESER").createKey("/GetResrHeaderSet", {
				IvRsnum: this._reqId,
			}),
			urlParameters = {"$expand": "Items"};
			oModels.genericReadWithUrlParams("RESER",sKey, oComponent_TR,urlParameters).then(function (data) {
				if(!!data.Items.results.length){
					let aItems = data.Items.results;
					if(sStatus === 'TRANSIT'){
						aItems = aItems.filter((aObject) => {
							return aObject.Ablad === 'TRANSIT';
						});
					}
					for(let i = 0; i< aItems.length; i++){
						aItems[i].lineNum = aItems[i].Rspos;
						if(aItems[i].ResCode === '0000'){
							aItems[i].ResCode = '';
						}
						aItems[i].ResCode = (+(aItems[i].ResCode)).toString();
						aItems[i].Wempf = aItems[i].Wempf.replace( /\s/g, ''); //delete space 
						aReqItems.push(aItems[i]);
					}					
				}
				modelGenral.setProperty("/aReqItems",aReqItems);
				modelGenral.refresh();
				modelDetails.setProperty("/currReq/stocLocKey", data.Lgort);
				modelDetails.setProperty("/currReq/reqToDate", data.Bdter);
				modelDetails.setProperty("/currReq/rsnumDes", data.Sgtxt);
				modelDetails.setProperty("/currReq/statusReq", data.Ablad);
				// if(data.Ablad === 'APPROVED'){
				// 	MessageBox.warning(oComponent_TR.i18n("msgEditDisable"), {
				// 		actions: [oComponent_TR.i18n("CLOSE")],
				// 		emphasizedAction: oComponent_TR.i18n("CLOSE"),
				// 	});	
				// }
				modelDetails.refresh();

			});
		},
		_onEditMatched: function (oEvent) {
			this._reqId = oEvent.getParameter("arguments").reqId;
			oComponent_TR.getModel("Details").setProperty("/reqId",this._reqId);
			oComponent_TR._fcl.byId("fcl").setLayout("TwoColumnsMidExpanded");
			oComponent_TR._fcl._updateUIElements();		

			let generalModel = oComponent_TR.getModel("General");
			generalModel.setProperty("/general/mode", "edit");
			generalModel.setProperty("/dispalyItemsTable", true);

			//get header and items for rsum selection screen
			this.getRsnumDetails();
		},
		goToHome: function (oEvent) {
			oComponent_TR.getRouter().navTo("master");
		},
		onEditReq: function (oEvent) {
			var modelDetails = oComponent_TR.getModel("Details"),
				modelGenral = oComponent_TR.getModel("General"),
				//oCopyHeaderReq = Object.assign({}, modelDetails.getProperty("/currReq")),
				oCopyListReq = Object.assign({}, modelGenral.getProperty("/aReqItems"));

			//modelDetails.setProperty("/copyHeaderReq", oCopyHeaderReq);
			modelGenral.setProperty("/copyListReq", oCopyListReq);
			oComponent_TR.getRouter().navTo("edit_req", {reqId: modelDetails.getProperty("/reqId")});
		},
		onCancelEdit: function (oEvent) {
			var modelDetails = oComponent_TR.getModel("Details"),
				modelGenral = oComponent_TR.getModel("General");
			oComponent_TR.getRouter().navTo("view_req", {reqId: modelDetails.getProperty("/reqId")});
			//modelDetails.setProperty("/currReq", modelDetails.getProperty("/copyHeaderReq"));
			modelGenral.setProperty("/aReqItems", modelDetails.getProperty("/copyListReq"));
		},
		onSaveChanges: function (oEvent) {
			var modelDetails = this.getModel("Details"),
			    modelGenral = this.getModel("General"),
				sStrogeLocation = modelDetails.getProperty("/currReq/stocLocKey"),
				reqToDate = this.getFixedDate(modelDetails.getProperty("/currReq/reqToDate")),
				reqNum = modelDetails.getProperty("/reqId"),
				aReqItems = modelGenral.getProperty("/aReqItems"),
				aItems = [],
				oEntry = {};

			aReqItems.forEach(oItem => {
				aItems.push({
						Material: oItem.Matnr,
						EntryQnt: (+oItem.Request).toFixed(3),
						EntryUom: oItem.Meins,
						ReqDate: reqToDate
					}
				)
			});
			oEntry = {
				IsHeader: {
					ReservNo: reqNum,
					MoveStloc: sStrogeLocation
				},
				ZuiMmImHandleReservation: {
					IvAction: "U"
				},
				EvReserv: "",
				Items: aItems,
				Return: []
			};
			oModels.genericDeepInsert("RESER", "/ResrHandleSet" ,oEntry, oComponent_TR).then(function (data) {
				oComponent_TR._MessageHandler.DataReturnMSG(data.Return.results);

				if(!oComponent_TR._MessageHandler.isErrorMsg(data.Return.results)){
					oComponent_TR._MasterController.getModel("RESER").refresh();
					oComponent_TR._detailController.getRsnumDetails();
					oComponent_TR.getRouter().navTo("view_req", {reqId: modelDetails.getProperty("/reqId")});
				 }
			});
		},
		onBeforeDeleteRes: function (oEvent) {
			MessageBox.warning(oComponent_TR.i18n("msgDeleteReq"), {
				actions: [oComponent_TR.i18n("YES"), oComponent_TR.i18n("NO")],
				emphasizedAction: oComponent_TR.i18n("YES"),
				onClose: function (sAction) {
					if(sAction === oComponent_TR.i18n("YES")){
						oComponent_TR._detailController.onDeleteReq();			
					}
				}
			});	
		},
		onDeleteReq: function () {
			var modelDetails = this.getModel("Details"),
			    modelGenral = this.getModel("General"),
				sStrogeLocation = modelDetails.getProperty("/currReq/stocLocKey"),
				reqToDate = this.getFixedDate(modelDetails.getProperty("/currReq/reqToDate")),
				reqNum = modelDetails.getProperty("/reqId"),
				aReqItems = modelGenral.getProperty("/aReqItems"),
				aItems = [],
				oEntry = {};

			aReqItems.forEach(oItem => {
				aItems.push({
						Material: oItem.Matnr,
						EntryQnt: (+oItem.Request).toFixed(3),
						EntryUom: oItem.Meins,
						ReqDate: reqToDate
					}
				)
			});
			oEntry = {
				IsHeader: {
					ReservNo: reqNum,
					MoveStloc: sStrogeLocation
				},
				ZuiMmImHandleReservation: {
					IvAction: "D"
				},
				EvReserv: "",
				Items: aItems,
				Return: []
			};
			oModels.genericDeepInsert("RESER", "/ResrHandleSet" ,oEntry, oComponent_TR).then(function (data) {
				//oComponent_TR._MessageHandler.DataReturnMSG(data.Return.results);
				//if(oComponent_TR._MessageHandler.isAllSuccessMsg(data.Return.results)){
					oComponent_TR._MasterController.getModel("RESER").refresh();
					oComponent_TR.getRouter().navTo("master");
				//} 
			});
		},
		onBeforeTransferStock: function(oEvent){
			var modelGenral = oComponent_TR._detailController.getModel("General"),
				sBiilOfLoad = modelGenral.getProperty("/billLoad"),
				sMsg = !sBiilOfLoad ? "msgBillLoad" : "msgTransferItems";
	
			MessageBox.warning(oComponent_TR.i18n(sMsg), {
				actions: [oComponent_TR.i18n("YES"), oComponent_TR.i18n("NO")],
				emphasizedAction: oComponent_TR.i18n("YES"),
				onClose: function (sAction) {
					if(sAction === oComponent_TR.i18n("YES")){
						oComponent_TR._detailController.onTransStock(oEvent);		
					}
				}
			});	
		},
		onTransStock: function(oEvent){
			var aSelectedItems = oComponent_TR._detailController.getView().byId("idTransTableItems").getSelectedItems(),
				modelDetails = this.getModel("Details"),
				reqNum = modelDetails.getProperty("/reqId"),
				modelGenral = oComponent_TR._detailController.getModel("General"),
				sBillLoad = modelGenral.getProperty("/billLoad"),
				aItems = [],
				oEntry = {};

				aSelectedItems.forEach(oItem => {
					let object = oItem.getBindingContext("General").getObject();
					aItems.push({
							Rspos: object.lineNum,
							Matnr: object.Matnr,
							Maktx: object.Maktx,
							Meins: object.Meins,
							RequiredQntSum: object.RequiredQntSum,
							MissingQnt: (+object.MissingQnt).toFixed(3),
							StockWOutSs: object.StockWOutSs,
							StockWSs: object.StockWSs,
							Request: (+object.Request).toFixed(3),
							Werks: object.Werks,
							Bdter: object.Bdter,
							Umlgo: object.Umlgo,
							Ausme: object.Ausme,
							SendStock: object.SendStock,
							RecStock: object.RecStock,
							UseType: object.UseType,
							ResCode: (+object.ResCode).toString(),
							Wempf:  (+object.Wempf).toFixed(3)
						}
					)
				});
				oEntry = {
					IvBillOfLoading: sBillLoad,
					IvRsnum: reqNum,
					Items: aItems,
					Return: []
				};
				oModels.genericDeepInsert("RESER", "/TransSupplySet" ,oEntry, oComponent_TR).then(function (data) {
						oComponent_TR._MessageHandler.DataReturnMSG(data.Return.results);

						if(!oComponent_TR._MessageHandler.isErrorMsg(data.Return.results)){
							const oNavigationButton = {
								semanticActionObj : "#",
								actionUrl :  "",
								semanticObj : "",
								oParams: {},
								bNewTab: false,
								ShowButton: true,
								ButtonText: oComponent_TR.i18n("moveToHome")
							};
				
							oComponent_TR._MessageHandler._DialogFragment.getModel("MessageManager").setProperty("/NavigationButton", oNavigationButton);
						}

				});

		},
		onBeforeConfrimStock: function(oEvent){
			MessageBox.warning(oComponent_TR.i18n("msgConfrimItems"), {
				actions: [oComponent_TR.i18n("YES"), oComponent_TR.i18n("NO")],
				emphasizedAction: oComponent_TR.i18n("YES"),
				onClose: function (sAction) {
					if(sAction === oComponent_TR.i18n("YES")){
						oComponent_TR._detailController.onConfrimStock();			
					}
				}
			});	
		},
		onConfrimStock: function(oEvent){
			var aSelectedItems = oComponent_TR._detailController.getView().byId("idConfirmTableItems").getSelectedItems(),
				modelDetails = this.getModel("Details"),
				reqNum = modelDetails.getProperty("/reqId"),
				aItems = [],
				oEntry = {};

				aSelectedItems.forEach(oItem => {
					let object = oItem.getBindingContext("General").getObject();
					aItems.push({
							Rspos: object.lineNum,
							Matnr: object.Matnr,
							Maktx: object.Maktx,
							Meins: object.Meins,
							RequiredQntSum: object.RequiredQntSum,
							MissingQnt: (+object.MissingQnt).toFixed(3),
							StockWOutSs: object.StockWOutSs,
							StockWSs: object.StockWSs,
							Request: object.Request,
							Werks: object.Werks,
							Bdter: object.Bdter,
							Umlgo: object.Umlgo,
							Ausme: object.Ausme,
							SendStock: object.SendStock,
							RecStock: object.RecStock,
							UseType: object.UseType,
							ResCode: (+object.ResCode).toString(),
							Wempf:  (+object.Wempf).toFixed(3)
						}
					)
				});
				oEntry = {
					IvRsnum: reqNum,
					Items: aItems,
					Return: []
				};
				oModels.genericDeepInsert("RESER", "/ApproveTransSet" ,oEntry, oComponent_TR).then(function (data) {
					oComponent_TR._MessageHandler.DataReturnMSG(data.Return.results);

					// if(!oComponent_TR._MessageHandler.isErrorMsg(data.Return.results)){
					// 	oComponent_TR._MasterController.getModel("RESER").refresh();
					// 	oComponent_TR._detailController.getRsnumDetails();
					// 	oComponent_TR.getRouter().navTo("view_req", {reqId: modelDetails.getProperty("/reqId")});
					// }

			});

		},
		onMngApproved: function(oEvent){
			var modelDetails = this.getModel("Details"),
				reqNum = modelDetails.getProperty("/reqId"),
				oEntry = {};
				oEntry = {
					IvRsnum: reqNum,
					Return: []
				};
				oModels.genericDeepInsert("RESER", "/MngApproveSet" ,oEntry, oComponent_TR).then(function (data) {
					oComponent_TR._MessageHandler.DataReturnMSG(data.Return.results);

					if(!oComponent_TR._MessageHandler.isErrorMsg(data.Return.results)){
						oComponent_TR._MasterController.getModel("RESER").refresh();
						oComponent_TR._detailController.getRsnumDetails();
						oComponent_TR.getRouter().navTo("view_req", {reqId: modelDetails.getProperty("/reqId")});
					}

			});

		},
		onCollectApprove: function(oEvent){
			var modelDetails = this.getModel("Details"),
				reqNum = modelDetails.getProperty("/reqId"),
				oEntry = {};
				oEntry = {
					IvRsnum: reqNum,
					Return: []
				};
				oModels.genericDeepInsert("RESER", "/PickApproveSet" ,oEntry, oComponent_TR).then(function (data) {
					oComponent_TR._MessageHandler.DataReturnMSG(data.Return.results);

					if(!oComponent_TR._MessageHandler.isErrorMsg(data.Return.results)){
						oComponent_TR._MasterController.getModel("RESER").refresh();
						oComponent_TR._detailController.getRsnumDetails();
						oComponent_TR.getRouter().navTo("view_req", {reqId: modelDetails.getProperty("/reqId")});
					}

			});

		},
		onChangeSelected: function (oEvent) {
			var model = oComponent_TR.getModel("General"),
				sStatus = model.getProperty("/status"),
				sIdTable = sStatus === 'ALL' ? 'idTableItems' : (sStatus === 'PICKED' ? 'idTransTableItems' : 'idConfirmTableItems'),				
				aSelected = oComponent_TR._detailController.getView().byId(sIdTable).getSelectedItems(),
				modelDetails = oComponent_TR._detailController.getModel("Details");
			if(aSelected.length === 0){
				modelDetails.setProperty("/transferStockEnabled",false);
				modelDetails.setProperty("/ConfrimStockEnabled",false);
			}else {
				modelDetails.setProperty("/transferStockEnabled",true);
				modelDetails.setProperty("/ConfrimStockEnabled",true);
			}
			modelDetails.refresh();
		},
	});
});