/* global oComponent_TR: true */
sap.ui.define([
	"ztransrequest/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/mvc/Controller"
], function (BaseController, JSONModel, Controller) {
	"use strict";
	return BaseController.extend("ztransrequest.controller.SelectEntry", {

		 onInit : function() {
			oComponent_TR._SelectEntryController = this;
			this.oRouter = this.getOwnerComponent().getRouter();
			this.oModel = this.getOwnerComponent().getModel();

			this.oRouter.getRoute("selectEntry").attachPatternMatched(this._onSelectEntryMatched, this);
		},
		_onSelectEntryMatched: function(oEvent){
			oComponent_TR._fcl.byId("fcl").setLayout("TwoColumnsMidExpanded");
			oComponent_TR._fcl._updateUIElements();
		}
	});
});
