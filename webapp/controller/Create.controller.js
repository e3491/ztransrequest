/* global oComponent_TR: true */
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"ztransrequest/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"

], function (Controller, BaseController, JSONModel, MessageBox, Filter, FilterOperator ) {
	"use strict";

	return BaseController.extend("ztransrequest.controller.Create", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf ztransrequest.view.Create
		 */
		onInit: function () {
			oComponent_TR._createController = this;
			this.oRouter = this.getOwnerComponent().getRouter();
			this.oModel = this.getOwnerComponent().getModel();

			this.oRouter.getRoute("create").attachPatternMatched(this._onCreateMatched, this);

		},
		_onCreateMatched: function (oEvent) {
			oComponent_TR.getModel("General").setProperty("/general/mode", "create");
			oComponent_TR._fcl.byId("fcl").setLayout("OneColumn");
			oComponent_TR._fcl._updateUIElements();

		},

		initialCreateByUserDetails: function(){
			let myDetailsModel = oComponent_TR.getModel("MyDetails"),
				createModel = oComponent_TR.getModel("Create"),
				sUserLagort = myDetailsModel.getProperty("/EvUserLgort");
				//sDateType = myDetailsModel.getProperty("/EvImDateFormat");

			if(!!sUserLagort){ // if lagort of user exist
				createModel.setProperty("/currReq/stocLocKey", sUserLagort);
				createModel.setProperty("/currReq/stocLocEnabled", false);
			}
		},
		onCancelCreateReq: function (oEvent) {
			MessageBox.warning(oComponent_TR.i18n("msgCancelCreate"), {
				actions: [oComponent_TR.i18n("YES"), oComponent_TR.i18n("NO")],
				emphasizedAction: oComponent_TR.i18n("YES"),
				onClose: function (sAction) {
					if(sAction === oComponent_TR.i18n("YES")){
						oComponent_TR.getRouter().navTo("master");
						oComponent_TR._createController.cleanCurrCreates();					
					}
				}
			});	
		},
		cleanCurrCreates: function (oEvent) {
			var createModel = oComponent_TR._createController.getModel("Create"),
				currentReq = createModel.getProperty("/currReq"),
				reqToReset = {
					stocLocKey : "",
					reqToDate: ""					
				};
				createModel.setProperty("/currReq", {...currentReq,...reqToReset});
		},
		onDisplayItemsData: function (oEvent) {
			var generalModel = this.getModel("General"),
				createModel = this.getModel("Create"),
				sLagort = createModel.getProperty("/currReq/stocLocKey"),
				reqToDate = createModel.getProperty("/currReq/reqToDate"),
				aReqItems = [],
				aFilters = [];

			generalModel.setProperty("/dispalyItemsTable", true);
			aFilters.push(new sap.ui.model.Filter({path: "IvLgort", operator: FilterOperator.EQ, value1: sLagort}));
			aFilters.push(new sap.ui.model.Filter({path: "IvBdter", operator: FilterOperator.EQ, value1: this.getFixedDate(reqToDate)}));

			//get items for Lagort and Date from selection screen
			oModels.genericQuery("RESER","/GetResrSumsSet", aFilters, oComponent_TR).then(function (data) {
				if(!!data.results.length){
					for(let i = 0; i< data.results.length; i++){
						data.results[i].lineNum = (i+1).toString();
						//data.results[i].Request = data.results[i].MissingQnt; //change 11.12.24
						aReqItems.push(data.results[i]);
					}					
				}
				generalModel.setProperty("/aReqItems",aReqItems);
				generalModel.refresh();
			});
	    },
		onCreateReq: function (oEvent) {
			var createModel = this.getModel("Create"),
				generalModel = this.getModel("General"),
				sStrogeLocation = createModel.getProperty("/currReq/stocLocKey"),
				reqToDate = this.getFixedDate(createModel.getProperty("/currReq/reqToDate")),
				aReqItems = generalModel.getProperty("/aReqItems"),
				aItems = [],
				oEntry = {};

			aReqItems.forEach(oItem => {
				aItems.push(
					{
						Material: oItem.Matnr,
						EntryQnt: (+oItem.Request).toFixed(3),
						EntryUom: oItem.Meins,
						ReqDate: reqToDate
					}
				)
			});
			oEntry = {
				IsHeader: {
					ReservNo: "",
					MoveStloc: sStrogeLocation
				},
				ZuiMmImHandleReservation: {
					IvAction: "C"
				},
				EvReserv: "",
				Items: aItems,
				Return: []
			};
			oModels.genericDeepInsert("RESER", "/ResrHandleSet" ,oEntry, oComponent_TR).then(function (data) {
				oComponent_TR._MessageHandler.DataReturnMSG(data.Return.results);
				if(!oComponent_TR._MessageHandler.isErrorMsg(data.Return.results)){
					oComponent_TR._createController.getModel("RESER").refresh();

					const oNavigationButton = {
						semanticActionObj : "ReservationsManagement-display",
						actionUrl :  "display&/View/" + data.EvReserv,
						semanticObj : "ReservationsManagement",
						oParams: {},
						bNewTab: false,
						ShowButton: true,
						ButtonText: oComponent_TR.i18n("moveToRes")
					};
		
					oComponent_TR._MessageHandler._DialogFragment.getModel("MessageManager").setProperty("/NavigationButton", oNavigationButton);
					//reset all fields
					createModel.setProperty("/currReq/stocLocKey", "");
					createModel.setProperty("/currReq/reqToDate", null);
					generalModel.setProperty("/aReqItems", []);

					// oComponent_TR.getRouter().navTo("view_req", {reqId: data.EvReserv});
				} 
			});
	},
		onCreateIssue: function (oEvent) {
			var createModel = this.getModel("Create"),
				generalModel = this.getModel("General"),
				sStrogeLocation = createModel.getProperty("/currReq/stocLocKey"),
				aReqItems = generalModel.getProperty("/aReqItems"),
				aItems = [],
				oEntry = {};

				aReqItems.forEach(oItem => {
					aItems.push({
							Matnr: oItem.Matnr,
							Maktx: oItem.Maktx,
							Meins: oItem.Meins,
							RequiredQntSum: oItem.RequiredQntSum,
							MissingQnt: (+oItem.MissingQnt).toFixed(3),
							StockWOutSs: oItem.StockWOutSs,
							StockWSs: oItem.StockWSs,
							Request: (+oItem.Request).toFixed(3),
							Werks: oItem.Werks,
							Bdter: new Date(),
							Umlgo: oItem.Umlgo,
							Ausme: oItem.Ausme,
							SendStock: oItem.SendStock,
							RecStock: oItem.RecStock,
							UseType: oItem.UseType,
							ResCode: (+oItem.ResCode).toString()
						}
					)
				});
				oEntry = {
					IvLgort: sStrogeLocation,
					Items: aItems,
					Return: []
				};
				oModels.genericDeepInsert("RESER", "/IssueFlightSet" ,oEntry, oComponent_TR).then(function (data) {
						oComponent_TR._MessageHandler.DataReturnMSG(data.Return.results);
					     if(!oComponent_TR._MessageHandler.isErrorMsg(data.Return.results)){
							const oNavigationButton = {
								semanticActionObj : "#",
								actionUrl :  "",
								semanticObj : "",
								oParams: {},
								bNewTab: false,
								ShowButton: true,
								ButtonText: oComponent_TR.i18n("moveToHome")
							};
				
							oComponent_TR._MessageHandler._DialogFragment.getModel("MessageManager").setProperty("/NavigationButton", oNavigationButton);
					}

					});
		}

	});

});