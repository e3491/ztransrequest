/* global oComponent_TR: true */
sap.ui.define([
	"ztransrequest/controller/BaseController",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'sap/ui/model/Sorter',
	'sap/m/MessageBox'

], function (BaseController, Controller, Filter, FilterOperator, Sorter, MessageBox) {
	"use strict";

	return BaseController.extend("ztransrequest.controller.Master", {
		onInit: function () {
			
			oComponent_TR._MasterController = this;

			this.oRouter = this.getOwnerComponent().getRouter();
			this.oRouter.getRoute("master").attachPatternMatched(this._onMasterMatched, this);
			this._bDescendingSort = false;
		},
		onListUpdateFinished: function (oEvent) {
			var sTitle,
				iTotalItems = oEvent.getParameter("total"),
				oViewModel = this.getModel("Master");

			// only update the counter if the length is final
			if (this.byId("ReqListID").getBinding("items").isLengthFinal()) {
				if (iTotalItems) {
					sTitle = this.getResourceBundle().getText("reqListCount", [iTotalItems]);
				} else {
					//Display 'Line Items' instead of 'Line items (0)'
					sTitle = this.getResourceBundle().getText("Title");
				}
				oViewModel.setProperty("/tableTitle", sTitle);
			}

		},
		onListItemPress: function (oEvent) {
			var oReqSelected = oEvent.getParameter("listItem").getBindingContext("RESER").getObject();
			this.oRouter.navTo("view_req", { reqId: oReqSelected.Rsnum});
		},
		onCreateReq: function (oEvent) {
			var generalModel = oComponent_TR.getModel("General"),
				MasterModel = oComponent_TR.getModel("Master");

			MasterModel.setProperty("/SelectedFilters/freeText", "");
			oComponent_TR._MasterController.getView().byId("ReqListID").getBinding("items").filter([]);
			generalModel.setProperty("/aReqItems", []);
			generalModel.setProperty("/mode", "create");
			this.oRouter.navTo("create");
		},
		onAdd: function (oEvent) {
			MessageBox.show("This functionality is not ready yet.", {
				icon: MessageBox.Icon.INFORMATION,
				title: "Aw, Snap!",
				actions: [MessageBox.Action.OK]
			});
		},
		_onMasterMatched: function(oEvent){
			oComponent_TR._fcl.byId("fcl").setLayout("TwoColumnsMidExpanded");
			oComponent_TR._fcl._updateUIElements();
			// oComponent_TR._MasterController.filterByStatusAndLgort();

		},
		filterByStatusAndLgort: function(){
				//If there is a storage site belonging to the user - only the requirements of this storage site are displayed and filter by status
				var myDetailsModel = oComponent_TR.getModel("MyDetails"),
				sUserLagort = myDetailsModel.getProperty("/EvUserLgort"),
				generalModel = oComponent_TR.getModel("General"),
				sStatus = generalModel.getProperty("/status"),
				aFilters = [];

			if(sStatus !== 'ALL' ){
				aFilters.push(new Filter({
					path: "Status",
					operator: FilterOperator.EQ,
					value1: sStatus
				}));
			}else{
				aFilters.push(new Filter({
					path: "Status",
					operator: FilterOperator.NE,
					value1: "COMPLETE"
				}));
			}
			if(!!aFilters.length){
				oComponent_TR._MasterController.getView().byId("ReqListID").getBinding("items").filter(aFilters);
			}

		},
		onSearch: function (oEvent) {
			oComponent_TR._MasterController.onFilterMasterList();
		},
		onSelectSLFinish: function(oEvent){
			oComponent_TR._MasterController.onFilterMasterList();
		},
		onFilterMasterList: function(){
			var aFilters = [],
				masterModel = oComponent_TR.getModel("Master"),
				sSearchValue = masterModel.getProperty("/SelectedFilters/freeText"),
				aLgorts = masterModel.getProperty("/SelectedFilters/SlKeys"),
				sStatusFilterKey = masterModel.getProperty("/SelectedFilters/statusKey"),
				myDetailsModel = oComponent_TR.getModel("MyDetails"),
				sUserLagort = myDetailsModel.getProperty("/EvUserLgort"),
				generalModel = oComponent_TR.getModel("General"),
				sStatus = generalModel.getProperty("/status");

			if (sSearchValue && sSearchValue.length > 0) {
				aFilters.push(new Filter({
					path: "IvReserv",
					operator: FilterOperator.EQ,
					value1: sSearchValue.trim()
				}));
			}
			if(!!sUserLagort){
				aFilters.push(new Filter({
					path: "Lgort",
					operator: FilterOperator.EQ,
					value1: sUserLagort
				}));

			}else if(aLgorts && !!aLgorts.length){
				aLgorts.forEach(sLgort => {
					aFilters.push(new Filter({
						path: "Lgort",
						operator: FilterOperator.EQ,
						value1: sLgort
					}));
	
				});
			}
			if(sStatus !== 'ALL'){
				aFilters.push(new Filter({
					path: "Status",
					operator: FilterOperator.EQ,
					value1: sStatus
				}));
			}else{
				sStatusFilterKey =  (sStatusFilterKey === '' || sStatusFilterKey === undefined) ? " " : sStatusFilterKey;
				if(sStatusFilterKey === 'ALL STATUS'){
					aFilters.push(new Filter({
						path: "Status",
						operator: FilterOperator.NE,
						value1: "COMPLETE"
					}));
				}else{
					aFilters.push(new Filter({
						path: "Status",
						operator: FilterOperator.EQ,
						value1: sStatusFilterKey
					}));
				}
				
			}

			oComponent_TR._MasterController.getView().byId("ReqListID").getBinding("items").filter(aFilters);

		}
	});
});