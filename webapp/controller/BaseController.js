/*global history */
/*global oComponent_TR true */

sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/core/UIComponent",
	"ztransrequest/model/formatter",
	"ztransrequest/model/models",
	"ztransrequest/model/popovers",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/export/Spreadsheet"

], function (Controller, History, UIComponent, formatter, models, popovers, Filter, FilterOperator, MessageBox, MessageToast, Spreadsheet) {
	"use strict";

	return Controller.extend("ztransrequest.controller.BaseController", {
		formatter: formatter,
		popovers: popovers,
		models: models,

		getRouter: function () {
			return UIComponent.getRouterFor(this);
		},

		getModel: function (sName) {
			return this.getView().getModel(sName);
		},

		setModel: function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		getText: function (sText) {
			return this.getModel("i18n").getResourceBundle().getText(sText);
		},
		/**
		 * Event handler  for navigating back.
		 * It checks if there is a history entry. If yes, history.go(-1) will happen.
		 * If not, it will replace the current entry of the browser history with the master route.
		 * @public
		 */
		navTo: function (psTarget, pmParameters, pbReplace) {
			this.getRouter().navTo(psTarget, pmParameters, pbReplace);
		},
		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},
		onNavBack: function () {
			var sPreviousHash = History.getInstance().getPreviousHash();

			if (sPreviousHash !== undefined) {
				// The history contains a previous entry
				history.go(-1);
			} else {
				// Otherwise we go backwards with a forward history
				var bReplace = true;
				this.getRouter().navTo("master", {}, bReplace);
			}
		},
		onOpenDialog: function (oController, sFragmentDialogName) {
			if (!oComponent_TR[oController][sFragmentDialogName]) {
				oComponent_TR[oController][sFragmentDialogName] = sap.ui.xmlfragment(sFragmentDialogName,
					"ztransrequest.view.fragments.Dialogs." +
					sFragmentDialogName, oComponent_TR[oController]);
				oComponent_TR[oController].getView().addDependent(oComponent_TR[oController][sFragmentDialogName]);
			}
			oComponent_TR[oController][sFragmentDialogName].open();
			oComponent_TR._currentController = oController;

		},
		onCloseDialog: function (oController, sDialogName) {
			if (oComponent_TR[oController][sDialogName]) {
				oComponent_TR[oController][sDialogName].close();
			}
		},
		onAddItem: function(oEvent){
			var modelGeneral = this.getModel("General"),
				aReqItems = modelGeneral.getProperty("/aReqItems"),
				sRowNum = 1,
				sLenItemsTable = aReqItems.length; 
			
			if( sLenItemsTable > 0){
				sRowNum = sLenItemsTable + 1;
			}
			var oItemToAdd = {
				lineNum: sRowNum.toString(),
				Matnr: "",
				Maktx: "",
				Meins: "",
				Request: "",
				RequiredQntSum: "",
				StockWOutSs: "",
				StockWSs: "",
				MissingQnt: "",
				Werks: "",
				Bdter: "",
				Umlgo: "",
				Ausme: "",
				SendStock: "0",
				RecStock: "0",
				UseType: "",
				ResCode: ""
			}
			aReqItems.push(oItemToAdd);
			modelGeneral.setProperty("/aReqItems", aReqItems);
			modelGeneral.refresh(true);
		},
		onBeforeDeleteItem: function (oEvent) {
			var mode = oComponent_TR.getModel("General").getProperty("/general/mode"),
				oPath = oEvent.getSource().getBindingContext("General").getPath();

			oComponent_TR.getModel("General").setProperty("/general/pathItem", oPath);

			if(mode === 'edit'){
				MessageBox.warning(oComponent_TR.i18n("msgDeleteItem"), {
					actions: [oComponent_TR.i18n("YES"), oComponent_TR.i18n("NO")],
					emphasizedAction: oComponent_TR.i18n("YES"),
					onClose: function (sAction) {
						if(sAction === oComponent_TR.i18n("YES")){
							oComponent_TR._detailController.onDelItemIconPress(oEvent);			
						}
					}
				});	
			}else{ //create mode
				oComponent_TR._createController.onDelItemIconPress(oEvent);
			}
		},
		onDelItemIconPress: function(oEvent){
			var modelGeneral = this.getModel("General"),
				aReqItems = modelGeneral.getProperty("/aReqItems"),
				oPath = oComponent_TR.getModel("General").getProperty("/general/pathItem"),
				oIdx = parseInt(oPath.substring(oPath.lastIndexOf("/") + 1));
				aReqItems.splice(oIdx, 1);

			for(let i = 0; i< aReqItems.length; i++){
				aReqItems[i].lineNum = (i+1).toString();
			}
			modelGeneral.setProperty("/aReqItems", aReqItems);
			modelGeneral.refresh(true);
		},
		onDeleteAllItems: function(oEvent){
			var modelGeneral = this.getModel("General");
				modelGeneral.setProperty("/aReqItems", []);
				modelGeneral.refresh(true);
		},	
		onDeleteSelectedItems: function(oEvent,bSaveAfter){
			const aTable = oEvent.getSource().getParent().getParent();
			const aSelectedItems = aTable.getSelectedItems(); // רשומות למיקה
			const aLineNumsToDelete = aSelectedItems.map(item => item.getBindingContext("General").getObject()['lineNum']); // שליפת הlinenum של הרשומות מחיקה
			const modelGeneral = this.getModel("General");
			const aItems = modelGeneral.getProperty("/aReqItems", []);
			const aNonDeletedItems = aItems.filter((obj) => !aLineNumsToDelete.includes(obj['lineNum'])); // סינון הרשימה שלא מסומנת למחיקה
			modelGeneral.setProperty("/aReqItems",aNonDeletedItems);
			aTable.removeSelections();
			oComponent_TR._detailController.onChangeSelected();
			
		},
		handleDecimal: function (value) {
			if (isNaN(value))
				value = value.replace(/[^\d.-]/g, '');
			if (!value) {
				/*	owner._detailController.getModel("detailView").setProperty("/Details/AdditionalData/ZannualCallyear", "");
					this.getView().getModel("detailView").refresh(true);*/
				return 0;
			}
			value = value.replace(",", "");
			var dec = value.indexOf(".");
			if ((dec !== -1) && (dec == value.length - 1))
				dec = ".";
			else if ((dec !== -1) && (dec == value.length - 2) && value[dec + 1] === "0")
				dec = ".0";
			else
				dec = "";

			if (value % 1 === 0) {
				value = parseInt(value).toLocaleString() + dec;
			} else {
				value = parseFloat(value).toLocaleString() + dec;
				if (value.indexOf(".") === value.length - 4)
					value = value.substring(0, value.length - 1);

			}
			return value;
		},
		handleDecimalQuantity: function (quantity) {
			if (isNaN(quantity))
				quantity = quantity.replace(/[^\d.-]/g, '');
			if (!quantity) {
				return 0;
			}
			quantity = quantity.replace(",", "");
			var dec = quantity.indexOf(".");
			if ((dec !== -1) && (dec == quantity.length - 1))
				dec = ".";
			else if ((dec !== -1) && (dec == quantity.length - 2) && quantity[dec + 1] === "0")
				dec = ".0";
			else if ((dec !== -1) && (dec == quantity.length - 3) && quantity[dec + 2] === "0")
				dec = quantity[dec + 1] == "0" ? ".00" : "0";

			else dec = "";

			if (quantity % 1 === 0) {
				quantity = parseInt(quantity).toLocaleString() + dec;
			} else {
				if (quantity.indexOf(".") === quantity.length - 5)
					quantity = quantity.substring(0, quantity.length - 1);
				quantity = parseFloat(quantity).toLocaleString() + dec;
			}

			return quantity;
		},
		goToHome: function (event) {
			var model = owner.getModel("JSON");
			if (owner._previousHref) { // if we came from WF
				window.location.replace(owner._previousHref);
				delete owner._previousHref;
			} else {
				var bReplace = true;
				this.getRouter().navTo("master", {}, bReplace);
			}
		},
		getFixedDate: function (value) {
			if (!value) {
				return;
			}
			var before = value.getTime();
			var after = new Date(before + 10800000); //  adding three hours to fix UTC gateway processing day back
			return after;
		},
		refreshJsonModel: function () {
			owner.getModel("JSON").refresh(true);
		},
		onExport: function (oEvent) {
			var aCols, oSettings, oSheet,
				aSelectedData = [],
				model = oComponent_TR.getModel("General"),
				sStatus = model.getProperty("/status"),
				aCols = this.createColumnConfig(),
				sIdTable = sStatus === 'ALL' ? 'idTableItems' : (sStatus === 'PICKED' ? 'idTransTableItems' : 'idConfirmTableItems'),
				aIndices = this.getView().byId(sIdTable).getBinding("items").aIndices;

			for (var i = 0; i < aIndices.length; i++) {
				debugger;
				var object = model.getProperty("/aReqItems/" + aIndices[i]);
				//display only whole numbers with commas
				object.Request = this.handleDecimal(object.Request);
				object.RequiredQntSum = this.handleDecimal(object.RequiredQntSum);
				object.StockWOutSs =  this.handleDecimal(object.StockWOutSs);
				object.StockWSs =  this.handleDecimal(object.StockWSs);
				object.MissingQnt = this.handleDecimal(object.MissingQnt);
				object.RecStock = this.handleDecimal(object.RecStock);
				object.ResCode = this.handleDecimal(object.ResCode);
				aSelectedData.push(object);
			}
			oSettings = {
				workbook: {
					columns: aCols
				},
				dataSource: aSelectedData,
				fileName: oComponent_TR.i18n('appTitle')
			};
			oSheet = new Spreadsheet(oSettings);
			oSheet.build()
				.then(function () {});
		},
		createColumnConfig: function () {
			var model = oComponent_TR.getModel("General"),
				sStatus = model.getProperty("/status"),
				sCreationType = model.getProperty("/CreationType");

			var aColumns = [];
			aColumns.push({
				label: oComponent_TR.i18n('line'),
				property: 'lineNum'
			});
			aColumns.push({
				label: oComponent_TR.i18n('matNumber'),
				property: 'Matnr'
			});
			aColumns.push({
				label: oComponent_TR.i18n('matDesc'),
				property: 'Maktx',
				width: '20rem'
			});
			// aColumns.push({
			// 	label: sStatus === 'ALL' ? oComponent_TR.i18n('basicUnit') : oComponent_TR.i18n('Unit'),
			// 	property: sStatus === 'ALL' ? 'Meins' : "Ausme"
			// });
			if(sStatus === 'ALL') {			
				aColumns.push({
					label: (sCreationType === 'Issuing' ? oComponent_TR.i18n('quantityIssue') : oComponent_TR.i18n('quantityReq')),
					property: 'Request'
				});
			}
			if(sCreationType === 'Res' && sStatus !== 'ALL'){
				aColumns.push({
					label: oComponent_TR.i18n('TransReq'),
					property: 'RequiredQntSum'
				});
			}
			// if(sCreationType === 'Res'){
			// 	aColumns.push({
			// 		label: sStatus === 'ALL' ? oComponent_TR.i18n('sysReq') : oComponent_TR.i18n('TransReq'),
			// 		property: 'RequiredQntSum'
			// 	});
			// }
			// if(sStatus === 'ALL') {
			// 	aColumns.push({
			// 		label: oComponent_TR.i18n('stockNoSs'),
			// 		property: 'StockWOutSs'
			// 	});
			// 	if(sCreationType === 'Res'){
			// 		aColumns.push({
			// 			label: oComponent_TR.i18n('stockSs'),
			// 			property: 'StockWSs'
			// 		});
			// 	}
			// 	aColumns.push({
			// 		label: oComponent_TR.i18n('lack'),
			// 		property: 'MissingQnt'
			// 	});
			// }else if (sStatus === 'TRANSIT'){
			// 	aColumns.push({
			// 		label: oComponent_TR.i18n('stockReceiver'),
			// 		property: 'RecStock'
			// 	});
			// 	aColumns.push({
			// 		label: oComponent_TR.i18n('resCode'),
			// 		property: 'ResCode'
			// 	});
			// }
			return aColumns;		
		},
		onSearchMatnr: function (oEvent) {
			var aFilters = [];
			var sQuery = oEvent.getParameter("value");

			if (sQuery.trim()) {
				aFilters.push(new Filter({
					filters: [
						new Filter({path:"Maktx", operator:FilterOperator.Contains, value1: sQuery.trim(), caseSensitive: false}),
						new Filter({path:"Matnr", operator: FilterOperator.Contains, value1: sQuery.trim(), caseSensitive: false})
					],
					and: false
				}));
			}
			oEvent.getSource().getBinding("items").filter(aFilters);
		},
		onSelectMatnr: function (oEvent) {
			var itemObject = oEvent.getParameter("selectedItem").getBindingContext('General').getObject(),
			 	modelGenral = oComponent_TR.getModel("General"),
				indexRow = modelGenral.getProperty("/indexItemRow"),
				mode = modelGenral.getProperty("/general/mode"),
				createModel = oComponent_TR.getModel("Create"),
				detailsModel = oComponent_TR.getModel("Details"),
				sCreationType = modelGenral.getProperty("/CreationType"),
				sLagort = '',
				reqToDate = '',
				aFilters = [];

			oEvent.getSource().getBinding("items").filter([]);
			modelGenral.setProperty("/aReqItems/"+ indexRow +"/Matnr", itemObject["Matnr"]);
			modelGenral.setProperty("/aReqItems/"+ indexRow +"/Maktx", itemObject["Maktx"]);

			if(mode === 'create'){
				sLagort = createModel.getProperty("/currReq/stocLocKey");
				reqToDate = createModel.getProperty("/currReq/reqToDate");

			}else{ //edit view
				sLagort = detailsModel.getProperty("/currReq/stocLocKey");
				reqToDate = detailsModel.getProperty("/currReq/reqToDate");
			}

			//get details of item for matnr
			aFilters.push(new sap.ui.model.Filter({path: "IvLgort", operator: FilterOperator.EQ, value1: sLagort}));
			if(sCreationType === 'Res'){ //יצירת דרישה
				aFilters.push(new sap.ui.model.Filter({path: "IvBdter", operator: FilterOperator.EQ, value1: this.getFixedDate(reqToDate)}));
			}else{ //יצירת ניפוק לטיסה
				aFilters.push(new sap.ui.model.Filter({path: "IvBdter", operator: FilterOperator.EQ, value1: this.getFixedDate(new Date(9999, 11, 31))}));
			}
			aFilters.push(new sap.ui.model.Filter({path: "IvMatnr", operator: FilterOperator.EQ, value1: itemObject["Matnr"]}));

			oModels.genericQuery("RESER","/GetResrSumsSet", aFilters, oComponent_TR).then(function (data) {
				if(!!data.results.length){
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/Meins" ,data.results[0].Meins);
					if(sCreationType === 'Res'){ // דרישה - יצירה או עדכון
						modelGenral.setProperty("/aReqItems/"+ indexRow + "/Request" ,data.results[0].Request);
					}else{ //מצב של ניפוק
						modelGenral.setProperty("/aReqItems/"+ indexRow + "/Request" ,"0.000");
					}
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/RequiredQntSum" ,data.results[0].RequiredQntSum);
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/StockWOutSs" ,data.results[0].StockWOutSs);
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/StockWSs" ,data.results[0].StockWSs);
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/MissingQnt" ,data.results[0].MissingQnt)
					if(sCreationType === 'Res'){ // דרישה - יצירה או עדכון
						//modelGenral.setProperty("/aReqItems/"+ indexRow + "/Request" ,data.results[0].MissingQnt);
						//modelGenral.setProperty("/aReqItems/"+ indexRow + "/MissingQnt" ,data.results[0].Request);
					}else{ //מצב של ניפוק
						modelGenral.setProperty("/aReqItems/"+ indexRow + "/MissingQnt" ,"0.000");
					}
				}else{
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/Meins" ,"");
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/Request" ,"0.000");
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/RequiredQntSum" ,"0.000");
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/StockWOutSs" ,"0.000");
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/StockWSs" ,"0.000");
					modelGenral.setProperty("/aReqItems/"+ indexRow + "/MissingQnt" ,"0.000");
				}
				modelGenral.refresh();
			});
			oEvent.getSource().getBinding("items").filter([]);
		},
		onCancelDialogMatnr: function (oEvent) {
			var modelGenral = oComponent_TR.getModel("General"),
			    mode = modelGenral.getProperty("/general/mode");
	
			if(mode === 'create'){
				this.onCloseDialog('_createController','MaterialSh');
			}else{ //edit view
				this.onCloseDialog('_detailController','MaterialSh');
			}
			oEvent.getSource().getBinding("items").filter([]);

		},
		onBeforeOpenDialog: function (oEvent, sDialogName) {
			var oController = '_createController',
				sPath = oEvent.getSource().getBindingContext("General").getPath().split("/")[2],
				modelGenral = oComponent_TR.getModel("General"),
			    mode = modelGenral.getProperty("/general/mode"),
				aFilters = [];


			if(mode !== 'create')
				oController = '_detailController';
			
			modelGenral.setProperty("/indexItemRow", sPath);

			let sModel =  oController === '_detailController' ? 'Details': 'Create',
				sLagortKey = oComponent_TR[oController].getModel(sModel).getProperty("/currReq/stocLocKey");

			aFilters.push(new Filter({path:"IvLgort", operator:FilterOperator.EQ, value1: (!!sLagortKey? sLagortKey:"")}));
			
			oModels.genericQuery("RESER","/GetMatnrListSet", aFilters, oComponent_TR).then(function (data) {

				if(!!data.results.length){
					modelGenral.setProperty("/aMatnrList",data.results);
				}
				oComponent_TR[oController].onOpenDialog(oController, sDialogName);
			});
			
		},
		onLiveChangeRequest: function (oEvent) {
			// var InputReq = oEvent.getSource().getValue().replace(/[^\d.-]/g, ''),
			// 	sPath = oEvent.getSource().getBindingContext("General").getPath(),
			// 	sStockWOutSs = (this.getModel("General").getProperty(sPath + "/StockWOutSs")).replace(/[^\d.-]/g, ''),

			// 	iMissingQnt = (+sStockWOutSs) - (+InputReq);

			// if(iMissingQnt < 0){
			// 	iMissingQnt = Math.abs(iMissingQnt);
			// }else{
			// 	iMissingQnt = "0.000";
			// }
			// this.getModel("General").setProperty(sPath+ "/MissingQnt" , iMissingQnt);
			// this.getModel("General").refresh();
		}
	});
});