var oComponent_TR,
	oModels,
	isAttachRequest = true; 
	
sap.ui.define([
	"sap/base/util/UriParameters",
	"sap/ui/core/UIComponent",
	"sap/ui/model/json/JSONModel",
	"sap/f/library",
	"sap/f/FlexibleColumnLayoutSemanticHelper",
	"ztransrequest/model/models",
	"ztransrequest/model/formatter",
	"ztransrequest/model/popovers"

], function(UriParameters, UIComponent, JSONModel, library, FlexibleColumnLayoutSemanticHelper, models, Formatters, Popovers) {
	"use strict";

	var LayoutType = library.LayoutType;

	var Component = UIComponent.extend("ztransrequest.Component", {

		formatter : Formatters,
		popover   : Popovers,

		metadata: {
			manifest: "json"
		},

		init: async function () {

			oComponent_TR = this;
			oModels = models;
			
			//To remove
			//Set Right to Left
			//sap.ui.getCore().getConfiguration().setRTL(true);
			// Set HE language
			//sap.ui.getCore().getConfiguration().setLanguage("iw_IL");
			// set the device model 
			var oModel = new JSONModel();
			this.setModel(oModel);
			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			// set the FLP model
			this.setModel(models.createFLPModel(), "FLP");
			// attach busy indicator to oData models
			models.attachModelEvents();

			// set others app models
			this.setModel(models.createGeneralModel(), 		  "General");
			this.setModel(models.createMasterModel(), 		  "Master");
			this.setModel(models.createDetailsModel(), 		  "Details");
			this.setModel(models.createCreaterModel(), 		  "Create");

			try{
				var sStatus =  !!this.getComponentData().startupParameters['STATUS'] ? this.getComponentData().startupParameters['STATUS'][0] : 'ALL',
					sCreationType = !!this.getComponentData().startupParameters['CreationType'] ? this.getComponentData().startupParameters['CreationType'][0] : 'Res',
					genralModel = oComponent_TR.getModel("General");

					genralModel.setProperty("/status", sStatus); // ALL, PICKED, TRANSIT 
					genralModel.setProperty("/CreationType", sCreationType) // Res/Issuing

				if(sCreationType === 'Issuing')
					genralModel.setProperty("/dispalyItemsTable", true);

			}catch(e){	
				var genralModel = oComponent_TR.getModel("General");
				genralModel.setProperty("/status", 'ALL');
				genralModel.setProperty("/CreationType", 'Res'); // Res/Issuing
			}

			UIComponent.prototype.init.apply(this, arguments);
			this.getRouter().initialize();
			
			models.genericRead("CA", "/MyDetailsSet('')", oComponent_TR).then(function (data) {
				oComponent_TR.setModel(new JSONModel(data), "MyDetails");
				const sMode = oComponent_TR.getModel("General").getProperty("/general/mode");
				if(sMode == 'create'){
					oComponent_TR._createController.initialCreateByUserDetails();
				}else{//view or edit
					oComponent_TR._MasterController.filterByStatusAndLgort();
				}
			});
			this.getModel().setSizeLimit(1000);
			this.getModel("CA").setSizeLimit(1000);

			try {
				oComponent_TR._MessageHandler = await this.loadComponents("zmessagehandler", "/sap/bc/ui5_ui5/sap/zmessagehandler/"); // msg
			} catch (error) {
			}

		},

		/**
		 * Returns an instance of the semantic helper
		 * @returns {sap.f.FlexibleColumnLayoutSemanticHelper} An instance of the semantic helper
		 */
		getHelper: function() {
			var oFCL = this.getRootControl().byId("fcl"),
				// oParams = UriParameters.fromQuery(location.search),
				oSettings = {
					defaultTwoColumnLayoutType: LayoutType.TwoColumnsMidExpanded,
					defaultThreeColumnLayoutType: LayoutType.ThreeColumnsMidExpandedEndHidden,
					mode: "ThreeColumnsMidExpandedEndHidden",
					initialColumnsCount: 3,
					maxColumnsCount: 3
				};

			return FlexibleColumnLayoutSemanticHelper.getInstanceFor(oFCL, oSettings);
		},
		i18n: function (text) {
			return oComponent_TR.getModel("i18n").getProperty(text);
		},
		loadComponents: async function (sID, sURL) {
			return sap.ui.component({
				componentData: {
					component: this
				},
				name: sID,
				async: true,
				url: sURL
			}).then(function (oComponent) {
				return oComponent;
			});

		},

	});
	return Component;
});